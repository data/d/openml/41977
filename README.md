# OpenML dataset: TuningSVMs

https://www.openml.org/d/41977

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Rafael G. Mantovani, Edesio Alcobaça, André L. D. Rossi, Joaquin Vanschoren, André C. P. L. F. de Carvalho  
**Source**: "A meta-learning recommender system for hyperparameter tuning: predicting when tuning improves SVM classifiers" - Information Sciences, volume 501, 2019.  
**Please cite**: 10.1016/j.ins.2019.06.005  

This is a meta-dataset which describes the SVM hyperparameter tuning problem. The target attribute indicates whether tuning is required or default hyperparameter values are enough to each dataset (row). Targets were defined using a statistical labelling rule comparing the predictive performance of models induced with defaults values and tuned ones. In this version of the dataset, the labelling rule was set with 95% confidence.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41977) of an [OpenML dataset](https://www.openml.org/d/41977). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41977/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41977/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41977/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

